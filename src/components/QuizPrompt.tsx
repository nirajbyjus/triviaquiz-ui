import React, { useState } from "react";
import { API_URL } from "../config";

export default function QuizPrompt({ questions, closeIframe, userId }: any) {
  console.log({ questions });
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [showScore, setShowScore] = useState(false);
  const [score, setScore] = useState(0);
  const [scoreMessage, setScoreMessage] = useState("");

  const handleAnswerOptionClick = (isCorrect: boolean) => {
    if (isCorrect) {
      setScore(score + 1);
    }

    const nextQuestion = currentQuestion + 1;
    if (nextQuestion < questions.length) {
      setCurrentQuestion(nextQuestion);
    } else {
      let percentage = (score / questions.length) * 100;
      let localStorage = window.localStorage;
      let today = new Date() as any;
      let dd = String(today.getDate()).padStart(2, "0");
      let mm = String(today.getMonth() + 1).padStart(2, "0");
      let yyyy = today.getFullYear();

      today = yyyy + "-" + mm + "-" + dd;

      let tqs = localStorage.getItem("tqs") as any;
      if (tqs) {
        tqs = JSON.parse(tqs);
      } else {
        tqs = [];
      }

      if (tqs.length) {
        tqs = [...tqs, { date: today, score: percentage + "%" }];
      } else {
        tqs = [{ date: today, score: percentage + "%" }];
      }
      localStorage.setItem("tqs", JSON.stringify(tqs));
      saveScoreInDatabase(score * 2, today);
      if (percentage > 80) {
        setScoreMessage(`excellent, we'll meet again tomorrow`);
      } else {
        setScoreMessage(
          `"No worries! We'll do better next time". Let's meet tomorrow`
        );
      }
      setShowScore(true);
      setTimeout(() => {
        closeIframe();
      }, 10000);
    }
  };

  const saveScoreInDatabase = (score: number, today: string) => {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let raw = JSON.stringify({
      score,
      userId: userId,
      scoreDate: today,
    });

    let requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    } as any;

    fetch(`${API_URL}/score`, requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log("error", error));
  };

  return (
    <>
      <div className="quiz">
        {showScore ? (
          <>
            <div className="score-section">
              <h4>
                You scored {score} out of {questions.length}
              </h4>
              <h3>{scoreMessage}</h3>
              <span>*Quiz will close in 10 seconds.</span>
            </div>
          </>
        ) : (
          <>
            <div className="question-section">
              <div className="question-count">
                <span>Question {currentQuestion + 1}</span>/{questions.length}
              </div>
              <div className="question-text">
                {questions[currentQuestion]?.questionText}
              </div>
            </div>
            <div className="answer-section">
              {questions[currentQuestion]?.answerOptions.map(
                (answerOption: any) => (
                  <button
                    className="btn1"
                    onClick={() =>
                      handleAnswerOptionClick(answerOption?.isCorrect)
                    }
                  >
                    {answerOption?.answerText}
                  </button>
                )
              )}
            </div>
          </>
        )}
      </div>
      <div style={{ visibility: "hidden" }}>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolor optio
        blanditiis, est minima quas consequatur architecto, quia voluptatum
        impedit nulla aspernatur molestias totam.
      </div>
    </>
  );
}
