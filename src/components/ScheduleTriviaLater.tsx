import React from "react";

export default function ScheduleTriviaLater({
  maximizeIframe,
  minimizeIframe,
  closeIframe,
  sendScheduleRegisteredMessage,
}: any) {
  const setScheduler = (minute: number) => {
    setTimeout(() => {
      maximizeIframe();
    }, minute * 60000);

    sendScheduleRegisteredMessage(minute)
    setTimeout(() => {
      closeIframe();
    }, 2000);
  };
  return (
    <div className="message-container user-message-container">
      <button className="user-btn" onClick={() => setScheduler(10)}>
        10
      </button>
      <button className="user-btn" onClick={() => setScheduler(30)}>
        30
      </button>
      <button className="user-btn" onClick={() => setScheduler(60)}>
        60
      </button>
      <button className="user-btn" onClick={() => closeIframe()}>
        no
      </button>
    </div>
  );
}
