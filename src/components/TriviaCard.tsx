import React from "react";
import '../stylesheets/triviacard.css'

export default function TriviaCard(props : any) {
  return (
    <div>
      <div className="courses-container">
        <div className="course">
          <div className="course-preview">
            <img
              width={200}
              height={"100%"}
              src={props.data.imageLink}
            />
          </div>
          <div className="course-info">
            <h2>{props.data.title}</h2>
            <p>
             {props.data.description}
            </p>
          </div>
        </div>
      </div>
      {/* <div className="floating-text">
        Your{" "}
        <a
          href="https://florin-pop.com/blog/2019/09/100-days-100-projects"
          target="_blank"
        >
          Score Card
        </a>
      </div> */}
    </div>
  );
}
