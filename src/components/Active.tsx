import React, { useState, useEffect } from "react";
import Chatbot from "../ChatBox";
import TriviaCard from "./TriviaCard";

const Active = (props: any) => {
  return (
    <div
      className="h-100 w-100"
      style={{ border: "2px solid #002f50 ", paddingTop: "9vmin" }}
    >
      <div className="h-100 overflow-auto">
        <Chatbot
          minimizeIframe={props.minimizeIframe}
          closeIframe={props.closeIframe}
          maximizeIframe={props.maximizeIframe}
        />
      </div>
    </div>
  );
};

export default Active;
