import React from "react";

export default function UserMessage({
  onClickYes,
  onClickNo,
  disableButton,
}: any) {
  const [disable, setDisable] = React.useState(false);
  console.log({ disableButton });
  return (
    <div className="message-container user-message-container">
      <button
        className="user-btn"
        onClick={() => {
          setDisable(true);
          onClickYes();
        }}
        disabled={disable}
      >
        Yes
      </button>
      <button className="user-btn" onClick={()=>{
        setDisable(true)
        onClickNo()
      }} disabled={disable}>
        No
      </button>
    </div>
  );
}
