import React, { useState, useEffect, useContext, useRef } from "react";
import BotMessage from "./components/BotMessage";
import UserMessage from "./components/UserMessage";
import Messages from "./components/Messages";

import API from "./ChatbotAPI";

import QuizPrompt from "./components/QuizPrompt";
import "./styles.css";
import "./index.css";
import { Context } from "./context/context";
import TriviaCard from "./components/TriviaCard";
import { API_URL } from "./config";
import ScheduleTriviaLater from "./components/ScheduleTriviaLater";

function Chatbot(props: any) {
  const [messages, setMessages] = useState([]);
  const [quizQuestions, setQuizQestions] = useState([]);

  const quizQuestionsRef = useRef([]);
  
  quizQuestionsRef.current = quizQuestions;

  const messagesRef = useRef(messages);
  messagesRef.current = messages;

  const userData = JSON.parse(useContext(Context));

  useEffect(() => {
    async function loadWelcomeMessage() {
      setMessages([
        <BotMessage
          key="0"
          fetchMessage={async () =>
            await API.GetChatbotResponse("welcome", userData.studentName)
          }
        />,
      ] as any);
      getQuizData();
    }
    loadWelcomeMessage();
  }, []);

  const getQuizData = () => {
    fetch(`${API_URL}/trivia?type=${userData.courseName.toLowerCase()}&limit=3`)
      .then((response) => response.json())
      .then((data) => {
        console.log({ data });
        let triviaData = data;
        if (filterQuizData && filterQuizData.length) {
          triviaData.forEach((item: any, i: number) => {
            callTitleSetTimeout(item, i);
          });

          transformQuizQestions(triviaData);
          let timeoutForQuestion =
            triviaData && triviaData.length < 3 ? 18000 : 30000;
          setTimeout(() => {
            sendStaticMessage("ask_to_take_quiz");
          }, timeoutForQuestion);

          setTimeout(() => {
            sendUserData();
          }, timeoutForQuestion + 1000);
        }
      })
      .catch((err) => console.log({ err }));
  };

  const transformQuizQestions = (triviaData: any) => {
    let ids = triviaData.map((item: any) => item._id);
    if (ids && ids.length) {
      ids = ids.join(",");
    }

    const data = window.localStorage.getItem("triviaQuizQuestions");

    let parsedData;
    if (data) {
      parsedData = JSON.parse(data);
    }
    const now = new Date();
    if (parsedData && parsedData.expiry && now.getTime() > parsedData.expiry) {
      localStorage.removeItem("triviaQuizQuestions");
      parsedData = null;
    }
    console.log({ data, parsedData });
    if (data && parsedData && parsedData.questions) {
      let newQuizQuestions = quizQuestionsRef.current.concat(
        parsedData.questions
      );
      setQuizQestions(newQuizQuestions);
    } else {
      fetch(`${API_URL}/questions?triviaIds=${ids}`)
        .then((response) => response.json())
        .then((questions) => {
          if (questions && questions.length) {
            // Shuffle array
            const shuffled = questions.sort(() => 0.5 - Math.random());
            // Get sub-array of first n elements after shuffled
            let selected = shuffled.slice(0, 5);
            let transformedQuestions = transformIntoQuizFormat(selected);
            if (transformedQuestions && transformedQuestions.length) {
              let newQuizQuestions =
                quizQuestionsRef.current.concat(transformedQuestions);
              setQuestionsInLocalStorage(newQuizQuestions);
              setQuizQestions(newQuizQuestions);
            }
          }
        });
    }
  };

  const transformIntoQuizFormat = (selectedQuestions: any[]) => {
    let questions = [] as any;
    console.log({ selectedQuestions });
    selectedQuestions.forEach((item) => {
      let questionFormat = {} as any;
      if (item && item.question) {
        questionFormat.questionText = item.question;
      }
      if (item && item.answers && item.answers.length) {
        questionFormat.answerOptions = item.answers.map((item: any) => ({
          answerText: item.options,
          isCorrect: item.isCorrect,
        }));
      }
      questions.push(questionFormat);
    });

    return questions;
  };

  const setQuestionsInLocalStorage = (questions: any) => {
    const now = new Date();
    const data = { questions, expiry: now.getTime() + 1000 * 60 * 60 * 4 };
    window.localStorage.setItem("triviaQuizQuestions", JSON.stringify(data));
  };

  const filterQuizData = (data: any) => {
    if (data && data.length) {
      return data.filter((item: any) => {
        if (item && item.Category && item.Category.name) {
          if (item.Category.name === userData.courseName) {
            return true;
          } else {
            return false;
          }
        }
      });
    }
    return null;
  };

  const getTimeOut = (i: number) => {
    switch (i) {
      case 0:
        return 4000;
      case 1:
        return 18000;
      case 2:
        return 30000;
      default:
        return 0;
    }
  };

  const callTitleSetTimeout = (data: any, i: number) => {
    let titleTimeout = getTimeOut(i) - 2000;

    setTimeout(() => {
      sendTitle(data.title);
    }, titleTimeout);

    let cardTimeout = getTimeOut(i);
    setTimeout(() => {
      sendTriviaCard(data);
    }, cardTimeout);
  };

  const sendUserData = () => {
    let newMessages = messagesRef.current.concat(
      (
        <UserMessage
          key={messages.length + 1}
          onClickYes={onClickYes}
          onClickNo={onClickNo}
        
        />
      ) as any
    );
    setMessages(newMessages);
  };

  const onClickNo = () => {
    sendStaticMessage("ask_to_remind");
    setTimeout(() => {
      sendScheduler();
    }, 2000);
  };

  const sendScheduleRegisteredMessage = (time: number) => {
    const newMessages = messagesRef.current.concat(
      (
        <BotMessage
          key={messages.length + 2}
          fetchMessage={async () =>
            await API.GetChatbotResponse("scheduled_for_quiz", "", "", time)
          }
        />
      ) as any
    );
    setMessages(newMessages);
  };

  const sendScheduler = () => {
    let newMessages = messagesRef.current.concat(
      (
        <ScheduleTriviaLater
          maximizeIframe={props.maximizeIframe}
          minimizeIframe={props.minimizeIframe}
          closeIframe={props.closeIframe}
          sendScheduleRegisteredMessage={sendScheduleRegisteredMessage}
        />
      ) as any
    );
    setMessages(newMessages);
  };

  const onClickYes = () => {
    let newMessages = messagesRef.current.concat(
      (
        <QuizPrompt
          questions={quizQuestionsRef.current}
          closeIframe={props.closeIframe}
          userId={userData.studentId}
        />
      ) as any
    );
    setMessages(newMessages);
  };

  const sendStaticMessage = (message: string) => {
    const newMessages = messagesRef.current.concat(
      (
        <BotMessage
          key={messages.length + 2}
          fetchMessage={async () => await API.GetChatbotResponse(message)}
        />
      ) as any
    );
    setMessages(newMessages);
  };
  const sendTitle = async (title: string) => {
    const newMessages = messagesRef.current.concat(
      (
        <BotMessage
          key={messages.length + 2}
          fetchMessage={async () =>
            await API.GetChatbotResponse("title", userData.studentName, title)
          }
        />
      ) as any
    );
    setMessages(newMessages);
  };

  const sendTriviaCard = async (data: any) => {
    const newMessages = messagesRef.current.concat(
      (<TriviaCard data={data} />) as any
    );
    await setMessages(newMessages);
  };
  return (
    <div className="chatbot">
      <Messages messages={messages} />
    </div>
  );
}

export default Chatbot;
