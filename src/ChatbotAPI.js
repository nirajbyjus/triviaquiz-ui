const API = {
  GetChatbotResponse: async (message, student_name = '', title = '',time=0) => {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        if (message === "welcome") resolve(`Hello ${student_name}! Welcome to Trivia Quiz!`);
        if (message === 'show_trivia_card') resolve(`Did you know about <trivia_title>?`);
        if (message === 'title') resolve(`Did you know about ${title}?`)
        if (message === 'ask_to_take_quiz') resolve(`Would you like to take a quiz?`);
        if (message === 'ask_to_remind') resolve(`would you like me to remind you in [10, 30, 60, no] minutes?`);
        if(message === 'scheduled_for_quiz') resolve(`Great Will Remind you in ${time} minutes`)
        else resolve("echo : " + message);
      }, 0);
    });
  }
};

export default API;
