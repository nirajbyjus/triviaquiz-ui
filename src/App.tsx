import React, { useContext, useRef, useState } from "react";
import { Context } from "./context/context";
import Active from "./components/Active";
import TriviaCard from "./components/TriviaCard";

const App: React.FC = (props) => {
  const userData = JSON.parse(useContext(Context));
  const { courseName, studentName, studentId, headerColor } = userData;
  const [isMinimized, setIsMinimized] = useState(true);

  console.log("nirajkvinit", "iframeside in app", userData);

  const closeIframe = () => {
    window.parent.postMessage(
      JSON.stringify({
        identifier: "triviaquiz",
        data: { action: "closeIframe" },
      }),
      "*"
    );
  };

  const minimizeIframe = () => {
    setIsMinimized(true);
    window.parent.postMessage(
      JSON.stringify({
        identifier: "triviaquiz",
        data: { action: "minimizeIframe" },
      }),
      "*"
    );
  };
  console.log({isMinimized})

  const maximizeIframe = () => {
    console.log('calling maximize')
    setIsMinimized(false);
    window.parent.postMessage(
      JSON.stringify({
        identifier: "triviaquiz",
        data: { action: "maximizeIframe" },
      }),
      "*"
    );
  };

  const renderHeader = () => {
    return (
      <div
        className={`header d-flex flex-row  p-1 pt-1 m-0 text-white`}
      >
        <h3
          className={`text-white flex-grow-1 ${isMinimized ? "blink_me" : ""}`}
        >
          {/* {studentName && studentName} - Did you know? */}
          &nbsp;Trivia
        </h3>
        <div className="justify-content-end">
          {isMinimized ? (
            <button
              type="button"
              className="btn text-white"
              onClick={maximizeIframe}
            >
              <span className="material-icons-round">open_in_full</span>
            </button>
          ) : (
            <button
              type="button"
              className="btn text-white"
              onClick={minimizeIframe}
            >
              <span className="material-icons-round">close_fullscreen</span>
            </button>
          )}
          <button
            type="button"
            className="btn text-white"
            onClick={closeIframe}
          >
            <span className="material-icons-round">close</span>
          </button>
        </div>
      </div>
    );
  };

  const renderComponent = () => {
    return <Active config={{ email: "nirajkvinit" }} minimizeIframe={minimizeIframe} closeIframe={closeIframe} maximizeIframe={maximizeIframe}/>;
  };

  const renderUi = () => {
    if (isMinimized) {
      return <div className="w-100 mainBorder rounded">{renderHeader()}</div>;
    } else {
      return (
        <div className=" w-100 mainBorder rounded">
          {renderHeader()}
          {renderComponent()}
        </div>
      );
    }
  };

  return renderUi();
};

export default App;
