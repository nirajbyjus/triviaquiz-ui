export const quizData = [
    {
        "id": 1,
        "title": "The Starry Night",
        "description": "The Starry Night is an oil-on-canvas painting by the Dutch Post-Impressionist painter Vincent van Gogh. Painted in June 1889, it depicts the view from the east-facing window of his asylum room at Saint-Rémy-de-Provence, just before sunrise, with the addition of an imaginary village.",
        "imageLink": "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Van_Gogh_-_Starry_Night_-_Google_Art_Project-x0-y0.jpg/480px-Van_Gogh_-_Starry_Night_-_Google_Art_Project-x0-y0.jpg",
        "meta": null,
        "createdAt": "2021-08-12T09:25:13.000Z",
        "updatedAt": "2021-08-12T09:25:13.000Z",
        "CategoryId": 1,
        "Category": {
            "id": 1,
            "name": "Arts",
            "description": "arts",
            "imageLink": "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Van_Gogh_-_Starry_Night_-_Google_Art_Project-x0-y0.jpg/480px-Van_Gogh_-_Starry_Night_-_Google_Art_Project-x0-y0.jpg",
            "createdAt": "2021-08-12T09:25:13.000Z",
            "updatedAt": "2021-08-12T09:25:13.000Z"
        },
        "QuizQuestions": [
            {
                "id": 1,
                "question": "Who painted \"The Starry Night\" painting?",
                "level": "easy",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 1,
                "QuizAnswers": [
                    {
                        "id": 1,
                        "answer": "Thomas Edison",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 1
                    },
                    {
                        "id": 2,
                        "answer": "Vincen van Gogh",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 1
                    },
                    {
                        "id": 3,
                        "answer": "Pablo Picasso",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 1
                    },
                    {
                        "id": 4,
                        "answer": "Rambrandt van Rijn",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 1
                    }
                ]
            },
            {
                "id": 2,
                "question": "What kind of painting \"The Starry night\" is?",
                "level": "easy",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 1,
                "QuizAnswers": [
                    {
                        "id": 5,
                        "answer": "Watercolor painting",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 2
                    },
                    {
                        "id": 6,
                        "answer": "Acrylic paining",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 2
                    },
                    {
                        "id": 7,
                        "answer": "Digital painting",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 2
                    },
                    {
                        "id": 8,
                        "answer": "Oil-on-canvas painting",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 2
                    }
                ]
            },
            {
                "id": 3,
                "question": "In which year, \"The Starry Night\" painting was painted?",
                "level": "medium",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 1,
                "QuizAnswers": [
                    {
                        "id": 9,
                        "answer": "1821",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 3
                    },
                    {
                        "id": 10,
                        "answer": "1632",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 3
                    },
                    {
                        "id": 11,
                        "answer": "1889",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 3
                    },
                    {
                        "id": 12,
                        "answer": "1952",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 3
                    }
                ]
            },
            {
                "id": 4,
                "question": "The style of \"The Starry Night\" painting is considered as ______",
                "level": "hard",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 1,
                "QuizAnswers": [
                    {
                        "id": 13,
                        "answer": "Impressionism",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 4
                    },
                    {
                        "id": 14,
                        "answer": "Abstract Art",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 4
                    },
                    {
                        "id": 15,
                        "answer": "Modern Art",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 4
                    },
                    {
                        "id": 16,
                        "answer": "Cubism",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 4
                    }
                ]
            }
        ]
    },
    {
        "id": 2,
        "title": "The Olympics",
        "description": "The Starry Night is an oil-on-canvas painting by the Dutch Post-Impressionist painter Vincent van Gogh. Painted in June 1889, it depicts the view from the east-facing window of his asylum room at Saint-Rémy-de-Provence, just before sunrise, with the addition of an imaginary village.",
        "imageLink": "https://thumbs-prod.si-cdn.com/8RkWhMEt46d2cXvXc5blUGOwUgQ=/800x600/filters:no_upscale()/https://public-media.si-cdn.com/filer/6d/ba/6dba3359-d79c-4703-85e8-7e9ee61209a8/screen_shot_2021-06-22_at_120735_pm.png",
        "meta": null,
        "createdAt": "2021-08-12T09:25:13.000Z",
        "updatedAt": "2021-08-12T09:25:13.000Z",
        "CategoryId": 1,
        "Category": {
            "id": 1,
            "name": "Arts",
            "description": "arts",
            "imageLink": "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Van_Gogh_-_Starry_Night_-_Google_Art_Project-x0-y0.jpg/480px-Van_Gogh_-_Starry_Night_-_Google_Art_Project-x0-y0.jpg",
            "createdAt": "2021-08-12T09:25:13.000Z",
            "updatedAt": "2021-08-12T09:25:13.000Z"
        },
        "QuizQuestions": [
            {
                "id": 5,
                "question": "Pi is the ratio of a circle's ______________",
                "level": "hard",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 2,
                "QuizAnswers": [
                    {
                        "id": 17,
                        "answer": "diameter to its circumference",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 5
                    },
                    {
                        "id": 18,
                        "answer": "area to its diameter",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 5
                    },
                    {
                        "id": 19,
                        "answer": "circumference to its diameter",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 5
                    },
                    {
                        "id": 20,
                        "answer": "radius to the cube of its area",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 5
                    }
                ]
            },
            {
                "id": 6,
                "question": "Pi is irrational. This means that _________________",
                "level": "medium",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 2,
                "QuizAnswers": [
                    {
                        "id": 21,
                        "answer": "it cannot be expressed as a fraction of integers",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 6
                    },
                    {
                        "id": 22,
                        "answer": "it is only divisible by 111",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 6
                    },
                    {
                        "id": 23,
                        "answer": "it has only zeroes to the right of the decimal",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 6
                    },
                    {
                        "id": 24,
                        "answer": "nothing in particular. All numbers are irrational because they have no physical manifestation.",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 6
                    }
                ]
            },
            {
                "id": 7,
                "question": "True or false: Pi's digits never repeat",
                "level": "easy",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 2,
                "QuizAnswers": [
                    {
                        "id": 25,
                        "answer": "True",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 7
                    },
                    {
                        "id": 26,
                        "answer": "False",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 7
                    }
                ]
            },
            {
                "id": 8,
                "question": "Which famous brainy fellow was born on Pi Day?",
                "level": "medium",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 2,
                "QuizAnswers": [
                    {
                        "id": 27,
                        "answer": "Isaac Newton",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 8
                    },
                    {
                        "id": 28,
                        "answer": "Albert Einstein",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 8
                    },
                    {
                        "id": 29,
                        "answer": "Johannes Kepler",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 8
                    },
                    {
                        "id": 30,
                        "answer": "Shahrukh Khan",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 8
                    }
                ]
            }
        ]
    },
    {
        "id": 3,
        "title": "Van Gogh",
        "description": "The Starry Night is an oil-on-canvas painting by the Dutch Post-Impressionist painter Vincent van Gogh. Painted in June 1889, it depicts the view from the east-facing window of his asylum room at Saint-Rémy-de-Provence, just before sunrise, with the addition of an imaginary village.",
        "imageLink": "https://www.biography.com/.image/t_share/MTY2NTIzMzc4MTI2MDM4MjM5/vincent_van_gogh_self_portrait_painting_musee_dorsay_via_wikimedia_commons_promojpg.jpg",
        "meta": null,
        "createdAt": "2021-08-12T09:25:13.000Z",
        "updatedAt": "2021-08-12T09:25:13.000Z",
        "CategoryId": 1,
        "Category": {
            "id": 1,
            "name": "Arts",
            "description": "arts",
            "imageLink": "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Van_Gogh_-_Starry_Night_-_Google_Art_Project-x0-y0.jpg/480px-Van_Gogh_-_Starry_Night_-_Google_Art_Project-x0-y0.jpg",
            "createdAt": "2021-08-12T09:25:13.000Z",
            "updatedAt": "2021-08-12T09:25:13.000Z"
        },
        "QuizQuestions": [
            {
                "id": 5,
                "question": "Pi is the ratio of a circle's ______________",
                "level": "hard",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 2,
                "QuizAnswers": [
                    {
                        "id": 17,
                        "answer": "diameter to its circumference",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 5
                    },
                    {
                        "id": 18,
                        "answer": "area to its diameter",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 5
                    },
                    {
                        "id": 19,
                        "answer": "circumference to its diameter",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 5
                    },
                    {
                        "id": 20,
                        "answer": "radius to the cube of its area",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 5
                    }
                ]
            },
            {
                "id": 6,
                "question": "Pi is irrational. This means that _________________",
                "level": "medium",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 2,
                "QuizAnswers": [
                    {
                        "id": 21,
                        "answer": "it cannot be expressed as a fraction of integers",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 6
                    },
                    {
                        "id": 22,
                        "answer": "it is only divisible by 111",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 6
                    },
                    {
                        "id": 23,
                        "answer": "it has only zeroes to the right of the decimal",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 6
                    },
                    {
                        "id": 24,
                        "answer": "nothing in particular. All numbers are irrational because they have no physical manifestation.",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 6
                    }
                ]
            },
            {
                "id": 7,
                "question": "True or false: Pi's digits never repeat",
                "level": "easy",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 2,
                "QuizAnswers": [
                    {
                        "id": 25,
                        "answer": "True",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 7
                    },
                    {
                        "id": 26,
                        "answer": "False",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 7
                    }
                ]
            },
            {
                "id": 8,
                "question": "Which famous brainy fellow was born on Pi Day?",
                "level": "medium",
                "score": 10,
                "createdAt": "2021-08-12T09:25:13.000Z",
                "updatedAt": "2021-08-12T09:25:13.000Z",
                "TriviumId": 2,
                "QuizAnswers": [
                    {
                        "id": 27,
                        "answer": "Isaac Newton",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 8
                    },
                    {
                        "id": 28,
                        "answer": "Albert Einstein",
                        "isCorrect": true,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 8
                    },
                    {
                        "id": 29,
                        "answer": "Johannes Kepler",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 8
                    },
                    {
                        "id": 30,
                        "answer": "Shahrukh Khan",
                        "isCorrect": false,
                        "createdAt": "2021-08-12T09:25:13.000Z",
                        "updatedAt": "2021-08-12T09:25:13.000Z",
                        "QuizQuestionId": 8
                    }
                ]
            }
        ]
    }
]